# System Security:

**sudo, su, chmod, setfacl**

- setfacl :

# Process Manage :

**w , top, ps, kill, pkill, pstree, killall**

- ps :  ps aux | grep nginx  
- pkill :  
- pstree : pstree
- killall : killall

# Users Manage:

**id, usermod, useradd, groupadd, userdel**

- id: 
- usermod:
- useradd:
- groupadd:
- userdel:

# File System :

**mount, umount, fsck, df, du**

- mount:
- umount:
- fsck :
- df :
- du :

# Network :

**curl, mail, elinks, telnet**

- curl :  curl -XGET www.baidu.com  , curl -XPOST 
- mail :
- telnet :
- elinks :

# Network Testing 

**ping , netstat , host**

- host :

# Configure Network

**hostname , ifconfig**

# File Operating

**unlink, rename , ln**

- unlink: 

- rename :

- ln :

# CronTab

- crontab -e : 0 0 * * * reboot :(restart the server daily)

# At

- at : at 2:00 tomorrow  
> /path/command
(Ctrl + D end)
